#ifndef GR_GAME_TYPES_H
#define GR_GAME_TYPES_H

#include "bn_common.h"

namespace gr
{
    enum class scene_type
    {
        MAP,
        TRADE
    };

    enum class trade_type
    {
        OXYGEN,
        CARBON,
        IRON
    };

    char trade_title[3]
    {
        'Oxygen',
        'Carbon',
        'Iron'        
    };
}

#endif
