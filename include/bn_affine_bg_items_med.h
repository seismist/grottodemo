#ifndef BN_AFFINE_BG_ITEMS_MED_H
#define BN_AFFINE_BG_ITEMS_MED_H

#include "bn_affine_bg_item.h"

//{{BLOCK(med_bn_graphics)

//======================================================================
//
//	med_bn_graphics, 1024x1024@8, 
//	+ palette 208 entries, not compressed
//	+ 235 tiles (t reduced) not compressed
//	+ affine map, not compressed, 128x128 
//	Total size: 416 + 15040 + 16384 = 31840
//
//	Time-stamp: 2021-04-25, 17:55:24
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.16
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_MED_BN_GRAPHICS_H
#define GRIT_MED_BN_GRAPHICS_H

#define med_bn_graphicsTilesLen 15040
extern const bn::tile med_bn_graphicsTiles[3760 / (sizeof(bn::tile) / sizeof(uint32_t))];

#define med_bn_graphicsMapLen 16384
extern const bn::affine_bg_map_cell med_bn_graphicsMap[16384];

#define med_bn_graphicsPalLen 416
extern const bn::color med_bn_graphicsPal[208];

#endif // GRIT_MED_BN_GRAPHICS_H

//}}BLOCK(med_bn_graphics)

namespace bn::affine_bg_items
{
    constexpr const affine_bg_item med(
            affine_bg_tiles_item(span<const tile>(med_bn_graphicsTiles, 470), compression_type::NONE), 
            bg_palette_item(span<const color>(med_bn_graphicsPal, 208), bpp_mode::BPP_8, compression_type::NONE),
            affine_bg_map_item(med_bn_graphicsMap[0], size(128, 128), compression_type::NONE));
}

#endif

