#ifndef BN_AFFINE_BG_ITEMS_LAND_H
#define BN_AFFINE_BG_ITEMS_LAND_H

#include "bn_affine_bg_item.h"

//{{BLOCK(land_bn_graphics)

//======================================================================
//
//	land_bn_graphics, 4096x4096@8, 
//	+ palette 80 entries, not compressed
//	+ 225 tiles (t reduced) not compressed
//	+ affine map, not compressed, 512x512 
//	Total size: 160 + 14400 + 262144 = 276704
//
//	Time-stamp: 2021-04-25, 17:05:35
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.16
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_LAND_BN_GRAPHICS_H
#define GRIT_LAND_BN_GRAPHICS_H

#define land_bn_graphicsTilesLen 14400
extern const bn::tile land_bn_graphicsTiles[3600 / (sizeof(bn::tile) / sizeof(uint32_t))];

#define land_bn_graphicsMapLen 262144
extern const bn::affine_bg_map_cell land_bn_graphicsMap[262144];

#define land_bn_graphicsPalLen 160
extern const bn::color land_bn_graphicsPal[80];

#endif // GRIT_LAND_BN_GRAPHICS_H

//}}BLOCK(land_bn_graphics)

namespace bn::affine_bg_items
{
    constexpr const affine_bg_item land(
            affine_bg_tiles_item(span<const tile>(land_bn_graphicsTiles, 450), compression_type::NONE), 
            bg_palette_item(span<const color>(land_bn_graphicsPal, 80), bpp_mode::BPP_8, compression_type::NONE),
            affine_bg_map_item(land_bn_graphicsMap[0], size(512, 512), compression_type::NONE));
}

#endif

