#ifndef BN_REGULAR_BG_ITEMS_TAVERN_H
#define BN_REGULAR_BG_ITEMS_TAVERN_H

#include "bn_regular_bg_item.h"

//{{BLOCK(tavern_bn_graphics)

//======================================================================
//
//	tavern_bn_graphics, 256x256@4, 
//	+ palette 16 entries, not compressed
//	+ 689 tiles (t|f|p reduced) not compressed
//	+ regular map (flat), not compressed, 32x32 
//	Total size: 32 + 22048 + 2048 = 24128
//
//	Time-stamp: 2021-05-02, 11:46:45
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.16
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_TAVERN_BN_GRAPHICS_H
#define GRIT_TAVERN_BN_GRAPHICS_H

#define tavern_bn_graphicsTilesLen 22048
extern const bn::tile tavern_bn_graphicsTiles[5512 / (sizeof(bn::tile) / sizeof(uint32_t))];

#define tavern_bn_graphicsMapLen 2048
extern const bn::regular_bg_map_cell tavern_bn_graphicsMap[1024];

#define tavern_bn_graphicsPalLen 32
extern const bn::color tavern_bn_graphicsPal[16];

#endif // GRIT_TAVERN_BN_GRAPHICS_H

//}}BLOCK(tavern_bn_graphics)

namespace bn::regular_bg_items
{
    constexpr const regular_bg_item tavern(
            regular_bg_tiles_item(span<const tile>(tavern_bn_graphicsTiles, 689), bpp_mode::BPP_4, compression_type::NONE), 
            bg_palette_item(span<const color>(tavern_bn_graphicsPal, 16), bpp_mode::BPP_4, compression_type::NONE),
            regular_bg_map_item(tavern_bn_graphicsMap[0], size(32, 32), compression_type::NONE));
}

#endif

