#ifndef BN_SPRITE_ITEMS_NINJA_H
#define BN_SPRITE_ITEMS_NINJA_H

#include "bn_sprite_item.h"

//{{BLOCK(ninja_bn_graphics)

//======================================================================
//
//	ninja_bn_graphics, 16x256@4, 
//	+ palette 16 entries, not compressed
//	+ 64 tiles not compressed
//	Total size: 32 + 2048 = 2080
//
//	Time-stamp: 2021-04-22, 22:52:51
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.16
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_NINJA_BN_GRAPHICS_H
#define GRIT_NINJA_BN_GRAPHICS_H

#define ninja_bn_graphicsTilesLen 2048
extern const bn::tile ninja_bn_graphicsTiles[512 / (sizeof(bn::tile) / sizeof(uint32_t))];

#define ninja_bn_graphicsPalLen 32
extern const bn::color ninja_bn_graphicsPal[16];

#endif // GRIT_NINJA_BN_GRAPHICS_H

//}}BLOCK(ninja_bn_graphics)

namespace bn::sprite_items
{
    constexpr const sprite_item ninja(sprite_shape_size(sprite_shape::SQUARE, sprite_size::NORMAL), 
            sprite_tiles_item(span<const tile>(ninja_bn_graphicsTiles, 64), bpp_mode::BPP_4, compression_type::NONE, 16), 
            sprite_palette_item(span<const color>(ninja_bn_graphicsPal, 16), bpp_mode::BPP_4, compression_type::NONE));
}

#endif

