<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.6.0" name="craft" tilewidth="32" tileheight="32" tilecount="144" columns="12">
 <image source="../../../Downloads/craftpix-net-280167-free-level-map-pixel-art-assets-pack/1 Tiles/Map_tiles.png" trans="000000" width="384" height="384"/>
 <tile id="0" type="water"/>
 <tile id="1" type="water"/>
 <tile id="2" type="water"/>
 <tile id="3" type="green"/>
 <tile id="4" type="green"/>
 <tile id="5" type="green"/>
 <tile id="6" type="grey"/>
 <tile id="7" type="grey"/>
 <tile id="8" type="grey"/>
 <tile id="9" type="lightgreen"/>
 <tile id="10" type="lightgreen"/>
 <tile id="11" type="lightgreen"/>
 <tile id="12" type="water"/>
 <tile id="13" type="water"/>
 <tile id="14" type="water"/>
 <tile id="15" type="green"/>
 <tile id="16" type="green"/>
 <tile id="17" type="green"/>
 <tile id="18" type="grey"/>
 <tile id="19" type="grey"/>
 <tile id="20" type="grey"/>
 <tile id="21" type="lightgreen"/>
 <tile id="22" type="lightgreen"/>
 <tile id="23" type="lightgreen"/>
 <tile id="24" type="water"/>
 <tile id="25" type="water"/>
 <tile id="26" type="water"/>
 <tile id="27" type="green"/>
 <tile id="28" type="green"/>
 <tile id="29" type="green"/>
 <tile id="30" type="grey"/>
 <tile id="31" type="grey"/>
 <tile id="32" type="grey"/>
 <tile id="33" type="lightgreen"/>
 <tile id="34" type="lightgreen"/>
 <tile id="35" type="lightgreen"/>
 <tile id="36" type="water"/>
 <tile id="37" type="water"/>
 <tile id="38" type="water"/>
 <tile id="39" type="green"/>
 <tile id="40" type="green"/>
 <tile id="41" type="green"/>
 <tile id="42" type="grey"/>
 <tile id="43" type="grey"/>
 <tile id="44" type="grey"/>
 <tile id="45" type="lightgreen"/>
 <tile id="46" type="lightgreen"/>
 <tile id="47" type="lightgreen"/>
 <tile id="48" type="water"/>
 <tile id="49" type="water"/>
 <tile id="50" type="water"/>
 <tile id="51" type="green"/>
 <tile id="52" type="green"/>
 <tile id="53" type="green"/>
 <tile id="54" type="grey"/>
 <tile id="55" type="grey"/>
 <tile id="56" type="grey"/>
 <tile id="57" type="lightgreen"/>
 <tile id="58" type="lightgreen"/>
 <tile id="59" type="lightgreen"/>
 <tile id="60" type="water"/>
 <tile id="61" type="water"/>
 <tile id="62" type="water"/>
 <tile id="63" type="green"/>
 <tile id="64" type="green"/>
 <tile id="65" type="green"/>
 <tile id="66" type="grey"/>
 <tile id="67" type="grey"/>
 <tile id="68" type="grey"/>
 <tile id="69" type="lightgreen"/>
 <tile id="70" type="lightgreen"/>
 <tile id="71" type="lightgreen"/>
 <tile id="72" type="ice"/>
 <tile id="73" type="ice"/>
 <tile id="74" type="ice"/>
 <tile id="75" type="green"/>
 <tile id="76" type="green"/>
 <tile id="77" type="green"/>
 <tile id="78" type="grey"/>
 <tile id="79" type="grey"/>
 <tile id="80" type="grey"/>
 <tile id="81" type="lightgreen"/>
 <tile id="82" type="lightgreen"/>
 <tile id="83" type="lightgreen"/>
 <tile id="84" type="ice"/>
 <tile id="85" type="ice"/>
 <tile id="86" type="ice"/>
 <tile id="87" type="ice"/>
 <tile id="88" type="ice"/>
 <tile id="89" type="ice"/>
 <tile id="90" type="stone"/>
 <tile id="91" type="stone"/>
 <tile id="92" type="stone"/>
 <tile id="93" type="wall"/>
 <tile id="94" type="wall"/>
 <tile id="95" type="wall"/>
 <tile id="96" type="lava"/>
 <tile id="97" type="lava"/>
 <tile id="98" type="lava"/>
 <tile id="99" type="lava"/>
 <tile id="100" type="stone"/>
 <tile id="101" type="stone"/>
 <tile id="102" type="stone"/>
 <tile id="103" type="stone"/>
 <tile id="104" type="stone"/>
 <tile id="105" type="wall"/>
 <tile id="106" type="wall"/>
 <tile id="107" type="wall"/>
 <tile id="108" type="lava"/>
 <tile id="109" type="lava"/>
 <tile id="110" type="lava"/>
 <tile id="111" type="lava"/>
 <tile id="112" type="stone"/>
 <tile id="113" type="stone"/>
 <tile id="114" type="stone"/>
 <tile id="115" type="stone"/>
 <tile id="116" type="stone"/>
 <tile id="117" type="wall"/>
 <tile id="118" type="wall"/>
 <tile id="119" type="wall"/>
 <tile id="120" type="lava"/>
 <tile id="121" type="lava"/>
 <tile id="122" type="lava"/>
 <tile id="123" type="lava"/>
 <tile id="124" type="stone"/>
 <tile id="125" type="stone"/>
 <tile id="126" type="stone"/>
 <tile id="127" type="stone"/>
 <tile id="128" type="stone"/>
 <tile id="129" type="bridge"/>
 <tile id="130" type="bridge"/>
 <tile id="131" type="bridge"/>
 <tile id="132" type="lava"/>
 <tile id="133" type="lava"/>
 <tile id="134" type="lava"/>
 <tile id="135" type="lava"/>
 <tile id="136" type="stone"/>
 <tile id="137" type="stone"/>
 <tile id="138" type="bridge"/>
 <tile id="139" type="bridge"/>
 <tile id="140" type="bridge"/>
 <tile id="141" type="bridge"/>
 <tile id="142" type="bridge"/>
</tileset>
