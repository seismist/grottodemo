#include "bn_core.h"
#include "bn_math.h"
#include "bn_keypad.h"
#include "bn_string.h"
#include "bn_display.h"
#include "bn_blending.h"
#include "bn_bg_palettes.h"
#include "bn_affine_bg_ptr.h"
#include "bn_affine_bg_actions.h"
#include "bn_regular_bg_ptr.h"
#include "bn_sprites_mosaic.h"
#include "bn_sprite_actions.h"
#include "bn_sprite_builder.h"
#include "bn_sprite_animate_actions.h"
#include "bn_sprite_text_generator.h"
#include "bn_affine_bg_pa_register_hbe_ptr.h"
#include "bn_affine_bg_pd_register_hbe_ptr.h"
#include "bn_affine_bg_dx_register_hbe_ptr.h"
#include "bn_affine_bg_dy_register_hbe_ptr.h"

#include "load_attributes.h"

#include "variable_8x16_sprite_font.h"
#include "bn_sprite_items_ninja.h"
#include "bn_affine_bg_items_med.h"
#include "bn_regular_bg_items_tavern.h"

namespace
{
    // Movement state    
    int ninjaX = 0;
    int ninjaY = 0;
    int ninjaGoX = 0;
    int ninjaGoY = 0;
    int ninjaVit = 100;

    // Text position
    constexpr const bn::fixed text_y_inc = 14;
    constexpr const bn::fixed text_y_limit = (bn::display::height() / 2) - text_y_inc;

    // A short hand for 2D movement
    union direction
    {
       struct
       {
          unsigned up: 1;
          unsigned down: 1;
          unsigned left: 1;
          unsigned right: 1;
       } keys;
       int data = 0;
    };

    void world_map_scene()
    {
        // Prepare some text
        bn::string<32> text;
        bn::ostringstream text_stream(text);
        bn::vector<bn::sprite_ptr, 32> text_sprites;
        bn::sprite_text_generator text_generator(variable_8x16_sprite_font);
        text_generator.set_center_alignment();
        text_generator.generate(0, text_y_limit - 32, "Demo!", text_sprites);

        // Load a sprite
        bn::sprite_ptr ninja_sprite = bn::sprite_items::ninja.create_sprite(0, 0);
        // Initial angle of rotation
        //ninja_sprite.set_rotation_angle(0);
        /* Other things we could set:
        .set_position(48, 24);
        .set_scale(2);
        .set_horizontal_flip(true);
        .set_mosaic_enabled(true);
        .set_blending_enabled(true);
        */
        // Four walk animations for 4 directions
        bn::sprite_animate_action<4> ninja_animate_action = bn::create_sprite_animate_action_forever(
                    ninja_sprite, 16, bn::sprite_items::ninja.tiles_item(), 0, 1, 2, 3);
        
        // Create background object
        bn::affine_bg_ptr land_bg = bn::affine_bg_items::med.create_bg(0, 0);
        // For rotation
        //land_bg.set_pivot_position(1432, 874);
        // Extents we can walk
        int x_limit = (land_bg.dimensions().width() - bn::display::width()) / 2;
        int y_limit = (land_bg.dimensions().height() - bn::display::height()) / 2;

        // Some kind of black magic to optimize the world map
        bn::unique_ptr<bn::array<bn::affine_bg_mat_attributes, bn::display::height()>> land_attributes_ptr(
            new bn::array<bn::affine_bg_mat_attributes, bn::display::height()>());
        bn::array<bn::affine_bg_mat_attributes, bn::display::height()>& land_attributes = *land_attributes_ptr;
        bn::affine_bg_pa_register_hbe_ptr land_pa_hbe =
            bn::affine_bg_pa_register_hbe_ptr::create(land_bg, land_attributes._data);
        bn::affine_bg_pd_register_hbe_ptr land_pd_hbe =
            bn::affine_bg_pd_register_hbe_ptr::create(land_bg, land_attributes._data);
        bn::affine_bg_dx_register_hbe_ptr land_dx_hbe =
            bn::affine_bg_dx_register_hbe_ptr::create(land_bg, land_attributes._data);
        bn::affine_bg_dy_register_hbe_ptr land_dy_hbe =
            bn::affine_bg_dy_register_hbe_ptr::create(land_bg, land_attributes._data);

        // Movement initialise
        bn::affine_bg_move_to_action land_move_to = bn::affine_bg_move_to_action(
            land_bg, 500, ninjaX, ninjaY);

        // Create animation state
        direction last_direction;
        last_direction.keys.down = true;
        bool first_frame = true;
        int inc = 2; // movement increment

        while(! bn::keypad::start_pressed())
        {
            direction new_direction;
            bool key_held = false; // if a button is held

            // Check change in direction
            if(bn::keypad::left_held())
            {
                land_bg.set_pivot_x(bn::max(land_bg.pivot_x().right_shift_integer() - inc, 0 - x_limit));
                new_direction.keys.left = true;
                key_held = true;
            }
            else if(bn::keypad::right_held())
            {
                land_bg.set_pivot_x(bn::min(land_bg.pivot_x().right_shift_integer() + inc, x_limit - 1));
                new_direction.keys.right = true;
                key_held = true;
            }
            // note 2 directions can be held simultaneously
            if(bn::keypad::up_held())
            {
                land_bg.set_pivot_y(bn::max(land_bg.pivot_y().right_shift_integer() - inc, 0 - y_limit));
                new_direction.keys.up = true;
                key_held = true;
            }
            else if(bn::keypad::down_held())
            {
                land_bg.set_pivot_y(bn::min(land_bg.pivot_y().right_shift_integer() + inc, y_limit - 1));
                new_direction.keys.down = true;
                key_held = true;
            }
            // chiclets
            if(bn::keypad::a_pressed())
            {
                land_move_to = bn::affine_bg_move_to_action(
                    land_bg, ninjaVit, ninjaGoX + 100, ninjaGoY + 100);
            }
            else if(bn::keypad::b_pressed())
            {
                land_move_to = bn::affine_bg_move_to_action(
                    land_bg, ninjaVit, ninjaGoX - 100, ninjaGoY - 100);
            }

            load_attributes(land_bg.mat_attributes(), land_attributes._data);

            if(first_frame)
            {
                land_pa_hbe.reload_attributes_ref();
                land_pd_hbe.reload_attributes_ref();
                first_frame = false;
            }

            land_dx_hbe.reload_attributes_ref();
            land_dy_hbe.reload_attributes_ref();

            if(key_held && last_direction.data != new_direction.data)
            {
                if(new_direction.keys.left)
                {
                    ninja_animate_action = bn::create_sprite_animate_action_forever(
                        ninja_sprite, 12, bn::sprite_items::ninja.tiles_item(), 8, 9, 10, 11);
                }
                else if(new_direction.keys.right)
                {
                    ninja_animate_action = bn::create_sprite_animate_action_forever(
                        ninja_sprite, 12, bn::sprite_items::ninja.tiles_item(), 12, 13, 14, 15);
                }

                if(new_direction.keys.up)
                {
                    ninja_animate_action = bn::create_sprite_animate_action_forever(
                        ninja_sprite, 12, bn::sprite_items::ninja.tiles_item(), 4, 5, 6, 7);
                }
                else if(new_direction.keys.down)
                {
                    ninja_animate_action = bn::create_sprite_animate_action_forever(
                        ninja_sprite, 12, bn::sprite_items::ninja.tiles_item(), 0, 1, 2, 3);
                }

                last_direction = new_direction;
            }
            
            // If we change position on screen
            ninja_sprite.set_position(ninjaX, ninjaY);

            // Update message
            text.clear();
            text_sprites.clear();
            if (land_bg.pivot_x() > -160 && land_bg.pivot_x() < -110 && land_bg.pivot_y() > -160 && land_bg.pivot_x() < -110) {
                return; // TODO: specify which scene goes next
            } else {
                text_stream.append(land_bg.pivot_x());
                text_stream.append(" x ");
                text_stream.append(land_bg.pivot_y());
            }
            text_generator.generate(0, -text_y_limit, text, text_sprites);
            
            // Animate the background
            if (!land_move_to.done()) {
                land_move_to.update();
            }

            // Send update signals
            ninja_animate_action.update();
            bn::core::update();
        }
    }

    
    void trader_joe_scene()
    {

        /*
        constexpr const bn::string_view info_text_lines[] = {
            "START: return to map",
        };

        info info("Regular BGs visibility actions", info_text_lines, text_generator);
        */

        bn::regular_bg_ptr green_bg = bn::regular_bg_items::tavern.create_bg(0, 0);

        while(! bn::keypad::start_pressed())
        {
            // Prepare some text
            bn::string<32> text;
            bn::ostringstream text_stream(text);
            bn::vector<bn::sprite_ptr, 32> text_sprites;
            bn::sprite_text_generator text_generator(variable_8x16_sprite_font);
            text_generator.set_center_alignment();
            text_generator.generate(0, text_y_limit - 32, "Demo!", text_sprites);

            bn::core::update();
        }
    }

}


int main()
{
    bn::core::init();

    // Grey = transparency
    bn::bg_palettes::set_transparent_color(bn::color(16, 16, 16));

    while(true)
    {
        world_map_scene();
        bn::core::update();

        trader_joe_scene();
        bn::core::update();
    }
}